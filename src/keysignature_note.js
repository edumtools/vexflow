import { Vex } from './vex';
import { Note } from './note';
import { KeySignature } from './keysignature';
import { Flow } from './tables';
import { Glyph } from './glyph';

// To enable logging for this class. Set `Vex.Flow.KeySignatureNote.DEBUG` to `true`.
function L(...args) { if (KeySignatureNote.DEBUG) Vex.L('Vex.Flow.KeySignatureNote', args); }

/** @constructor */
export class KeySignatureNote extends Note {
  static get CATEGORY() { return 'keysignaturenote'; }

  static get DEBUG() { return true; }

  constructor(keySpec, clef) {
    super({ duration: 'b' });
    this.setAttribute('type', 'KeySignatureNote');

    this.keySpec = keySpec;
    this.keySig = new KeySignature(this.keySpec);
    this.keySig.setClef(clef);

    {
      let width = 0;
      const accList = Flow.keySignature(this.keySpec);
      for (let i = 0; i < accList.length; ++i) {
        const accGlyphData = Flow.accidentalCodes(accList[i].type);
        const glyph = new Glyph(accGlyphData.code, this.keySig.glyphFontScale);
        width += glyph.getMetrics().width;
      }
      this.setWidth(width);
    }
    // Note properties
    this.ignore_ticks = true;
  }

  setClef(clef) {
    this.keySig.setClef(clef);
    return this;
  }


  getCategory() { return KeySignature.CATEGORY; }

  getGlyphWidth() {
    return this.keySig.getGlyphWidth();
  }

  setStave(stave) {
    super.setStave(stave);
    this.keySig.setStave(stave);
    return this;
  }

  getBoundingBox() {
    return this.keySig.getBoundingBox();
  }

  getWidth() {
    return this.width;
  }

  addToModifierContext() {
    /* overridden to ignore */
    return this;
  }

  preFormat() {
    /* overridden to ignore */
    this.setPreFormatted(true);
    return this;
  }

  draw() {
    this.checkContext();
    if (!this.stave) throw new Vex.RERR('NoStave', "Can't draw without a stave.");
    L('Rendering keySigNote line at: ', this.getAbsoluteX());
    this.keySig.setX(this.getAbsoluteX());
    this.keySig.setStave(this.stave);
    this.keySig.draw();
    this.setRendered();
  }

}
